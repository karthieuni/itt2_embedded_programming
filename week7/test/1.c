#define F_CPU 16000000UL //Set the clock frequency
#include <avr/io.h> //call IO library
#include <util/delay.h> //call delay library
//#include "IncFile.h"

void main(void)
{
   DDRB = 0xff;   //PORTB output
   PORTB = 0x00;  //PORTB '0'
   DDRD = 0x00;   //PORTD input
   PORTD = 0xff;  //PORTD internall pullup
{  
    while(PIND & (1<<PD0)); //button not pressed
	_delay_ms(250);          //button initially pressed

	while(!(PIND & (1<<PD0)));  
			PORTB |= (1 << PB0);
    _delay_ms(175);         //button initially released

	PORTB &= ~(1 << PB0);
}
