#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>

void wait_for_button()
{
	while(PIND & (1<<PD0)); //button not pressed
	PORTB |= (1 << PB5);
	_delay_ms(2000);
	//button initially pressed

	while(!(PIND & (1<<PD0)))  
	PORTB |= (1 << PB5);
    _delay_ms(175);
	_delay_ms(175);
	_delay_ms(175);
	_delay_ms(175);
	_delay_ms(175);        //button initially released

	PORTB &= ~(1 << PB5);
}

int main(void)

{
	DDRB = 0xff; //Output LED - PORTB 'output'
	PORTB = 0x00; //Input LED - PORTB '0'
	DDRD = 0x00; // PORTD input
	PORTD = 0xff; // PORTD internal pullup
	
	while(1)
	{
        wait_for_button();
    }

return(0);
}