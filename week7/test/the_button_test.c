#define F_CPU 16000000UL //Set the clock frequency
#include <avr/io.h> //call IO library
#include <util/delay.h> //call delay library

int main(void)
{
	DDRB |= (1<<5);	//Define PB5 As Output
	DDRB &= ~(1<<4); 
	
	PORTB |= (1<<5); //Setting starting state as HIGH
	
	
	while(1)
	{
		if(PINB & (1<<4))
		{
			PORTB |= (1<<5); // TURN PB0 ON
		}
		else
		{
			PORTB &= ~(1<<5); // TURN PB0 OFF
		}
	}
}