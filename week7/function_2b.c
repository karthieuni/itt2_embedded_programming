// Inspiration code from https://www.avrfreaks.net/forum/need-idea-turning-onoff-led-using-same-button

#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>

int main(void)

{
	DDRB = 0xff; //Output LED - PORTB 'output'
	PORTB = 0x00; //Input LED - PORTB '0'
	DDRD = 0x00; // PORTD input
	PORTD = 0xff; // PORTD internal pullup
	
	while(1)
	{
		 if (PIND & (1 << PD0))
		 {
			 _delay_ms(175);                 // debounce
			 PORTB ^= (1 << PB5);               // toggle portb - PB5
			 loop_until_bit_is_set(PIND,0); // wait until button released
			 _delay_ms(175);                 // debounce
		 }
	 }
return(0);
}