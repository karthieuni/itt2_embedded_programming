#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

char data= 'a';
char data2= 'a';
char data3= 'a';
char datarec;
int count = 0;

void setup(void)
{
	DDRB |= (1<<2)|(1<<3)|(1<<5);    // SCK, MOSI and SS as outputs
	DDRB &= ~(1<<4);                 // MISO as input
	DDRB |= (1<<1);                  // slave #2
	DDRB |= (1<<0);                  // slave #3
	
	DDRD |= (1<<3);                  //green LED
	DDRD |= (1<<5);                  //yellow LED
	DDRD |= (1<<6);                  //maxi
	
	PORTB |= (1<<1);                  // slave #2
	PORTB |= (1<<0);                  // slave #3

	SPCR |= (1<<MSTR);               // Set as Master
	SPCR |= (1<<SPR1);               // divided clock by 64
	SPCR |= (1<<SPE);                // Enable SPI
}

void timer1(void)
{
	cli();
	TCCR1B |= (1<< WGM12);
	TCCR1B |= (0b00000011); //prescaler 64
	TCNT1 = 0;
	OCR1A = 62499;
	TIMSK1 |= (1<< OCIE1A);
	sei();
}

int main(void)
{
    setup();
	timer1();
    while (1) 
    {
		
    }
}

ISR(TIMER1_COMPA_vect)
{
	count+=1;
	switch(count)
	{
		case 20:
			SPDR = data;
			while(!(SPSR & (1<<SPIF)));
			_delay_ms(50);
			datarec = SPDR;
			PORTB |= (1<<2);
			PORTB |= (1<<0);
			PORTB &= ~(1<<1);
		
			if(datarec == 'b')
			{
				PORTD ^= (1<<3);
			}
		
			SPDR = data2;
			while(!(SPSR & (1<<SPIF)));
			_delay_ms(50);
			datarec = SPDR;
			PORTB |= (1<<2);
			PORTB |= (1<<1);
			PORTB &= ~(1<<0);
		
			if(datarec == 'd')
			{
				PORTD ^= (1<<5);
			}
		
			SPDR = data3;
			while(!(SPSR & (1<<SPIF)));
			_delay_ms(50);
			datarec = SPDR;
			PORTB &= ~(1<<2);
			PORTB |= (1<<1);                  
			PORTB |= (1<<0);
		
			if(datarec == 'f')
			{
				PORTD ^= (1<<6);
			}
		
			count = 0;
			TCNT1 = 0;
	}
}