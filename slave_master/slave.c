// this program enables SPI communication and
// Sets the AVR into Master mode

/*
#include <avr/io.h>
#include "Yes.h"

int main (void)
{
	char volatile data = 'a';
	setup();
	
	while(1)
	{
		//Receive(data);
		SendToMaster(data);
	}
}
*/

#include <avr/io.h>
#include <avr/interrupt.h>

char volatile data = 0b00000000;

int main (void)
{
	//char volatile data = 0b00000000;

	DDRB &= ~((1<<2)|(1<<3)|(1<<5));   // SCK, MOSI and SS as inputs
	DDRB |= (1<<4) | (1<<DDB0);                    // MISO as output

	SPCR &= ~(1<<MSTR);                // Set as slave
	SPCR |= (1<<SPR0)|(1<<SPR1);       // divide clock by 128
	SPCR |= (1<<SPE);                  // Enable SPI
	SPCR |= (1<<SPIE);

	sei();
	
	while(1)
	{
		/*while(!(SPSR & (1<<SPIF)));    // wait until all data is received
		data = SPDR;                   // hurray, we now have our data
		if (data == 'a'){
			PORTB ^= (1<<PORTB0);
		}*/
		;	
	}
}
ISR (SPI_STC_vect)
{
    data = SPDR;
	if (data == 'a'){
		PORTB ^= (1<<PORTB0);
	}
    // do something with the received data
}